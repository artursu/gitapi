package gitapi

import (

  "math"
  "net/http"
  "time"
  "strconv"
  "bytes"
  "io"
  "encoding/json"
  "log"
  "sort"
  "fmt"

)


func HandlerCommits(w http.ResponseWriter, r* http.Request) {

	var projects []project
	var response commitsResponse
	var pages int

limit := r.FormValue("limit")
auth := r.FormValue("auth")

invokeWebhooks("commits", "limit: " + limit + ", auth: " + auth, time.Now().String())

if r.Method != "GET" {

	http.Error(w, "405 Method Not Authorized", http.StatusMethodNotAllowed)
	return

}

if limit == "" {
  limit = "5"
}

limitInt, err := strconv.ParseFloat(limit, 64)

if err != nil {
  log.Print(err)
}

if limitInt > 100 {
	pages = int(math.Ceil(limitInt/100.0))
} else {
	pages = 1
}

requestProjects(pages, &projects, auth)

requestCommits(w, r, &projects, auth)

response.Repos = projects
if auth != "" {
	response.Auth = "true"
} else {
	response.Auth = "false"
}

var buffer = new(bytes.Buffer)

encoder := json.NewEncoder(buffer)
encoder.Encode(response)

w.Header().Add("Content-Type", "application/json")
w.WriteHeader(http.StatusOK)

io.Copy(w, buffer)

}







func requestCommits(w http.ResponseWriter, r* http.Request, projects* []project, auth string) {

	for i := 0; i < len(*projects); i++ {

		id := strconv.Itoa((*projects)[i].Id)
		client := &http.Client{}

		var page int = 0

		for ;; {

			page++

			var commits []commit

			req, err := http.NewRequest("GET", projectsUrl + id + "/repository/commits?per_page=100&page=" + strconv.Itoa(page), nil)
			if err != nil {
				log.Print(err)
			}

			req.Header.Set("Private-Token", auth)
			res, err := client.Do(req)

			if err != nil {
				log.Print(err)
			}

			err = json.NewDecoder(res.Body).Decode(&commits)

			if err != nil {
				fmt.Println ("No commits found!")
				log.Print(err)
			}

			res.Body.Close()

			(*projects)[i].Commits =(*projects)[i].Commits + len(commits)
			if len(commits) < 100 {
				break
			}
		}

		sort.Slice((*projects)[:], func(i, j int) bool {
  		return (*projects)[i].Commits > (*projects)[j].Commits
		})

	}

}








func requestProjects (pages int, projects *[]project, auth string) {

	client := &http.Client{}

	for i := 1; i <= pages; i++{

		var temp []project

		req, err := http.NewRequest("GET", projectsUrl + "?per_page=100" + "&page=" + strconv.Itoa(i), nil)

		if err != nil {
			log.Print(err)
			return
		}

		req.Header.Set("Private-Token", auth)

		res, err := client.Do(req)

		if err != nil {
			log.Print(err)
			return
		}

		err = json.NewDecoder(res.Body).Decode(&temp)

		if err != nil {
			log.Print(err)
			return
		}

		defer res.Body.Close()

		for i := 0; i < len(temp); i++ {

			(*projects) = append ((*projects), temp[i])

		}

		//No sense in downloading empty pages
		if len(temp) == 0 {
			break
		}

	}
}
