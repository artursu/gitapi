package main

//  Importing necessary packages
import (
	"fmt"
	"log"
	"net/http"
	"os"
	"gitapi"
	"time"
)

func main() {

//Keeps track of time at start
  gitapi.Uptime = time.Now()

	//Gets port and checks if it's "80", if not port is set to be "80"
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		fmt.Println("$PORT not found! Setting to 8080")
	}

	//HandleFunc() for each endpoint
	http.HandleFunc("/commits", gitapi.HandlerCommits)
	http.HandleFunc("/languages", gitapi.HandlerLanguages)
	http.HandleFunc("/issues", gitapi.HandlerIssues)
	http.HandleFunc("/webhooks/", gitapi.HandlerWebhooks)
	http.HandleFunc("/status", gitapi.HandlerStatus)

	//Prints which port server is listening on
	fmt.Println("Listening on port " + port)

	//Initialises server
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
