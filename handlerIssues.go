package gitapi

import (

  "net/http"
  "time"
  "strconv"
  "bytes"
  "io"
  "encoding/json"
  "log"
  "sort"
  "io/ioutil"

)


func HandlerIssues(w http.ResponseWriter, r* http.Request) {

	var issuesPayload issuesPayload
	var authors []author
	var labels []label

	typeParam := r.FormValue("type")
	auth := r.FormValue("auth")

	invokeWebhooks("issues", "type: " + typeParam + ", auth: " + auth, time.Now().String())

	if r.Method != "GET" {

    http.Error(w, "405 Method Not Authorized", http.StatusMethodNotAllowed)
    return

  }

	if typeParam == "" {

		http.Error(w, "400 Specify type", http.StatusBadRequest)
		return

	}

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
	    log.Print(err)
	}

	//fmt.Printf("%s", b)

	err = json.Unmarshal(b, &issuesPayload)

	if err != nil {
		log.Print(err)
		return
	}

	id := findProject(issuesPayload.Project, auth)

	if id == "" {

		http.Error(w, "404 Not Found", http.StatusNotFound)
		return

	}

	if typeParam == "users" {
		var response issuesResponseUsers

		authors = sortUsers(id, auth)

		if auth != "" {
			response.Auth = "true"
		} else {
			response.Auth = "false"
		}

		response.Users = authors

		var buffer = new(bytes.Buffer)

		encoder := json.NewEncoder(buffer)
		encoder.Encode(response)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		io.Copy(w, buffer)


	} else if typeParam == "labels" {
		var response issuesResponseLabels

		labels = sortLabels(id, auth)

		if auth != "" {
			response.Auth = "true"
		} else {
			response.Auth = "false"
		}

		response.Labels = labels

		var buffer = new(bytes.Buffer)

		encoder := json.NewEncoder(buffer)
		encoder.Encode(response)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		io.Copy(w, buffer)

	} else {
		http.Error(w, "400 Bad type", http.StatusBadRequest)
		return
	}


}





//This is overload of requestProjects function
func requestProjectsByName (pages int, projects *[]projectByName, auth string) {

	client := &http.Client{}

	for i := 1; i <= pages; i++{

		var temp []projectByName

		req, err := http.NewRequest("GET", projectsUrl + "?per_page=100" + "&page=" + strconv.Itoa(i), nil)

		if err != nil {
			log.Print(err)
			return
		}

		req.Header.Set("Private-Token", auth)

		res, err := client.Do(req)

		if err != nil {
			log.Print(err)
			return
		}

		err = json.NewDecoder(res.Body).Decode(&temp)

		if err != nil {
			log.Print(err)
			return
		}

		defer res.Body.Close()

		for i := 0; i < len(temp); i++ {

			(*projects) = append ((*projects), temp[i])

		}

		//No sense in downloading empty pages
		if len(temp) == 0 {
			break
		}

	}
}







func findProject(projectName string, auth string) string {

	var projects []projectByName

	requestProjectsByName (10, &projects, auth)

	for i := 0; i < len(projects); i++ {

		if projects[i].Name_with_namespace == projectName {

			return strconv.Itoa(projects[i].Id)

		}

	}

	return ""

}








func sortUsers(id string, auth string) []author{

	var issues []issue
	var authors []author
	var newAuthor author

	client := &http.Client{}

	req, err := http.NewRequest("GET", projectsUrl + id + "/issues/?per_page=100", nil)
	//req, err := http.NewRequest("GET", "https://git.gvk.idi.ntnu.no/api/v4/projects/419/issues/?per_page=500", nil)

	if err != nil {
		log.Print(err)
	}

	if id == "0" {
		//delet
	}

	req.Header.Set("Private-Token", auth)

	res, err := client.Do(req)

	err = json.NewDecoder(res.Body).Decode(&issues)

	if err != nil {
		log.Print(err)
	}

	for i := 0; i < len (issues); i++ {

		if !isInAuthors (issues[i].Author.Username, authors) {
			newAuthor.Username = issues[i].Author.Username
			newAuthor.Count = 1
			authors = append(authors, newAuthor)
		} else {
			for j := 0; j < len (authors); j++ {
				if authors[j].Username == issues[i].Author.Username {
					authors[j].Count++
				}
			}
		}

	}

	sort.Slice(authors[:], func(i, j int) bool {
		return authors[i].Count > authors[j].Count
	})

	return authors

}








func isInAuthors (username string, authors []author) bool {

	for i := 0; i < len (authors); i++ {
		if authors[i].Username == username {
			return true
		}
	}
	return false

}

func isInLabels (label string, labels []label) bool {

	for i := 0; i < len (labels); i++ {
		if labels[i].Label == label {
			return true
		}
	}
	return false

}

func isInEvents (event string) bool {

	for i := 0; i < len (events); i++ {
		if events[i] == event {
			return true
		}
	}
	return false

}








func sortLabels(id string, auth string) []label{

	var issues []issue
	var labels []label
	var newLabel label

	client := &http.Client{}

	req, err := http.NewRequest("GET", projectsUrl + id + "/issues/?per_page=100", nil)

	if err != nil {
		log.Print(err)
	}

	req.Header.Set("Private-Token", auth)

	res, err := client.Do(req)

	err = json.NewDecoder(res.Body).Decode(&issues)

	if err != nil {
		log.Print(err)
	}

//Goes into a nested for loops and sort all labels
	for i := 0; i < len (issues); i++ {


		for j := 0; j < len(issues[i].Labels); j++{


			if !isInLabels (issues[i].Labels[j], labels) {

				newLabel.Label = issues[i].Labels[j]
				newLabel.Count = 1
				labels = append(labels, newLabel)

			} else {


				for k := 0; k < len (labels); k++ {

					if labels[k].Label == issues[i].Labels[j] {

						labels[k].Count++

					}
				}
			}
	}

	}

	sort.Slice(labels[:], func(i, j int) bool {
		return labels[i].Count > labels[j].Count
	})

	return labels

}
