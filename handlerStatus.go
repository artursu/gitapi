package gitapi

import (

  "net/http"
  "io"
  "log"
  "strconv"
  "firebase.google.com/go"
  "google.golang.org/api/option"
  "context"
  "time"
  "bytes"
  "encoding/json"

)


func HandlerStatus(w http.ResponseWriter,r* http.Request){

  var stats status

  if r.Method != "GET" {

    http.Error(w, "405 Method Not Authorized", http.StatusMethodNotAllowed)
    return

  }

res, err := http.Get("https://git.gvk.idi.ntnu.no/api/v4/projects")

//Checks for errors
if err != nil {
  log.Print(err)
  //If request fails, sets status to 500
  stats.Gitlab = strconv.Itoa(http.StatusInternalServerError)
  return
}

//Sets status code retrieved from GBIF
stats.Gitlab = res.Status

ctx := context.Background()
sa := option.WithCredentialsFile("gitapi-artursu-b7a64b09ae87.json")
app, err := firebase.NewApp(ctx, nil, sa)
if err != nil {
  log.Print(err)
}

client, err := app.Firestore(ctx)
if err != nil {
  log.Print(err)
}
defer client.Close()

iter := client.Collection("test").Documents(ctx)
_, err = iter.Next()

if err != nil {
        stats.DB = strconv.Itoa(http.StatusInternalServerError)
}

//Sets status code retrieved from RESTcountries
stats.DB = strconv.Itoa(http.StatusOK)

//Closes body when function returns
defer res.Body.Close()

//Sets current version and uptime
stats.Version = version
stats.Uptime = strconv.FormatFloat(time.Since(Uptime).Seconds(), 'f', -1, 64)

  var buffer = new(bytes.Buffer)

  encoder := json.NewEncoder(buffer)
  encoder.Encode(stats)

  w.Header().Add("Content-Type", "application/json")
  w.WriteHeader(http.StatusOK)

  io.Copy(w, buffer)


}
