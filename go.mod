module gitapi

go 1.13

require (
	cloud.google.com/go v0.38.0
	firebase.google.com/go v3.10.0+incompatible
	google.golang.org/api v0.13.0
)
