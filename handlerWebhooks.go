package gitapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"firebase.google.com/go"
  "google.golang.org/api/option"
  "google.golang.org/api/iterator"
	"context"
	"strings"
	"time"
)



func HandlerWebhooks(w http.ResponseWriter, r* http.Request) {

	var webhookPayload webhook
//	var webhookResponse webhookInvoked

	if r.Method == http.MethodPost {


		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
		    log.Print(err)
		}

		//fmt.Printf("%s", b)

		err = json.Unmarshal(b, &webhookPayload)

		if err != nil {
			log.Print(err)
		}

		fmt. Println(webhookPayload.Event)
		fmt.Println(webhookPayload.URL)
		if (webhookPayload.Event == "") || (webhookPayload.URL == "") {

			http.Error(w, "400 Not enough information provided", http.StatusBadRequest)
			return

		}

		if !(isInEvents(webhookPayload.Event)){

			http.Error(w, "400 Non-existent event", http.StatusBadRequest)
			return

		}

		// Creating a Firebase connection
		ctx := context.Background()
		sa := option.WithCredentialsFile("gitapi-artursu-b7a64b09ae87.json")
		app, err := firebase.NewApp(ctx, nil, sa)
		if err != nil {
		  log.Print(err)
		}

		client, err := app.Firestore(ctx)
		if err != nil {
		  log.Print(err)
		}
		defer client.Close()

		fmt.Println("huj1")

		webhookPayload.Timestamp = time.Now().String()

		_, _, err = client.Collection("webhooks").Add(ctx, webhookPayload)
		if (err != nil) {

			log.Print(err)

		}

  } else if r.Method == http.MethodGet {

		parts := strings.Split(r.URL.Path, "/")
		fmt.Println("I hate my life so much rn" + parts[2])
		if parts[2] == ""{
			fmt.Println("hujhujhujhujhujhujhujhujhujhujhujhujhuj")
		}
		if len(parts) != 3 {
			http.Error(w, "Expecting format .../{webhookId} or .../", http.StatusBadRequest)
			return
		}
		fmt.Println(parts[2])

		//Connecting to Firebase
		ctx := context.Background()
		sa := option.WithCredentialsFile("gitapi-artursu-b7a64b09ae87.json")
		app, err := firebase.NewApp(ctx, nil, sa)
		if err != nil {
			log.Print(err)
		}

		client, err := app.Firestore(ctx)
		if err != nil {
			log.Print(err)
		}
		defer client.Close()

		if parts[2] != "" {
			fmt.Println("sdfgsgdsg")

			iter := client.Collection("webhooks").Documents(ctx)
			for {
							var temp webhook

							current, err := iter.Next()
							if err == iterator.Done {
											break
							}
							if err != nil {
											log.Print("Failed to iterate: %v", err)
							}

							if current.Ref.ID == parts[2]{

								current.DataTo(&temp)
								temp.Id = current.Ref.ID

								var buffer = new(bytes.Buffer)

								encoder := json.NewEncoder(buffer)
								encoder.Encode(temp)

								w.Header().Add("Content-Type", "application/json")
								w.WriteHeader(http.StatusOK)

								io.Copy(w, buffer)

								return

							}
						}
						http.Error(w, "404 Webhook not found!", http.StatusNotFound)
						return


			} else {

				var webhooksList []webhook

				fmt.Println("i hate this")

				// Creating a Firebase connection
				ctx := context.Background()
				sa := option.WithCredentialsFile("gitapi-artursu-b7a64b09ae87.json")
				app, err := firebase.NewApp(ctx, nil, sa)
				if err != nil {
				  log.Print(err)
				}

				client, err := app.Firestore(ctx)
				if err != nil {
				  log.Print(err)
				}
				defer client.Close()

				iter := client.Collection("webhooks").Documents(ctx)

				fmt.Println("i hate this2")
				for {
								var current webhook

				        doc, err := iter.Next()
				        if err == iterator.Done {
				                break
				        }
				        if err != nil {
				                log.Print("Failed to iterate: %v", err)
				        }

								fmt.Println("i hate this3")

								doc.DataTo(&current)
								current.Id = doc.Ref.ID

								webhooksList = append(webhooksList, current)

								//fmt.Println(current.Event)

				}

			fmt.Println("i hate this4")

			var buffer = new(bytes.Buffer)

			encoder := json.NewEncoder(buffer)
			encoder.Encode(webhooksList)

			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)

			io.Copy(w, buffer)



		}

	} else if r.Method == http.MethodDelete {

		parts := strings.Split(r.URL.Path, "/")
		//Connecting to Firebase
		ctx := context.Background()
		sa := option.WithCredentialsFile("gitapi-artursu-b7a64b09ae87.json")
		app, err := firebase.NewApp(ctx, nil, sa)
		if err != nil {
			log.Print(err)
		}

		client, err := app.Firestore(ctx)
		if err != nil {
			log.Print(err)
		}
		defer client.Close()

		if parts[2] != "" {
			fmt.Println("Imma delete stuff now")


			iter := client.Collection("webhooks").Documents(ctx)

			for {

							current, err := iter.Next()
							if err == iterator.Done {
											break
							}
							if err != nil {
											log.Print("Failed to iterate: %v", err)
							}

							fmt.Println(current.Ref.ID + " V " + parts[2])
							if current.Ref.ID == parts[2]{

								_, err := client.Collection("webhooks").Doc(parts[2]).Delete(ctx)
								if err != nil {
								        log.Print("An error has occurred: %s", err)
								}

								return

							}

						}
						http.Error(w, "404 Webhook not found", http.StatusNotFound)
						return


		} else {

			http.Error(w, "400 Expecting format .../{webhookId}", http.StatusBadRequest)
			return

		}



		}



	}

 	func invokeWebhooks(event string, params string, timestamp string) {

		//Connecting to Firebase
		ctx := context.Background()
		sa := option.WithCredentialsFile("gitapi-artursu-b7a64b09ae87.json")
		app, err := firebase.NewApp(ctx, nil, sa)
		if err != nil {
			log.Print(err)
		}

		firebaseClient, err := app.Firestore(ctx)
		if err != nil {
			log.Print(err)
		}
		defer firebaseClient.Close()

		iter := firebaseClient.Collection("webhooks").Documents(ctx)

		for {
						var temp webhook
						var webhookResponse webhookInvoked

						current, err := iter.Next()
						if err == iterator.Done {
										break
						}
						if err != nil {
										log.Print("Failed to iterate: %v", err)
						}

						current.DataTo(&temp)
						fmt.Println(temp.Event + " V " + event)
						if temp.Event == event{

							webhookResponse.Event = event
							webhookResponse.Params = params
							webhookResponse.Timestamp = timestamp

							client := &http.Client{}

							var buffer = new(bytes.Buffer)

							encoder := json.NewEncoder(buffer)
							encoder.Encode(webhookResponse)

							req, err := http.NewRequest("POST", temp.URL, buffer)
							if err != nil {
								log.Print(err)
							}

							req.Header.Set("Content-Type", "application/json")
							client.Do(req)

						}

					}


	}
