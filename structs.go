package gitapi

import "time"

const projectsUrl = "https://git.gvk.idi.ntnu.no/api/v4/projects/"
const version = "v1"

var Uptime time.Time //Creates a global time variable that is then used by main to retrieve time

var webhooks []webhook
var counter int = -1

var events = [4]string{"commits", "languages", "issues", "status"}

type project struct {

  Id int `json:id`
  Path_with_namespace string `json:path_with_namespace`
  Commits int `json:commits`

}

type projectByName struct {

  Id int `json:id`
  Name_with_namespace string `json:name_with_namespace`

}

type issue struct {

  Author author `json:author`
  Labels []string `json:labels`

}

type author struct {

  Username string `json:username`
  Count int `json:count`
}

type label struct {

  Label string `json:label`
  Count int `json:count`
}

type commit struct {

  //Id string `json:id`

}

type commitsResponse struct {

  Repos []project `json:repos`
  Auth string `json:auth`

}

type issuesResponseUsers struct {

  Users []author `json:users`
  Auth string `json:auth`

}

type issuesResponseLabels struct {

  Labels []label `json:labels`
  Auth string `json:auth`

}

type issuesPayload struct {

  Project string `json:project`

}

type languagePayload struct {

  Name []string

}

type webhook struct {

  Id string `json:id`
  Event string `json:event`
  URL string `json:url`
  Timestamp string `json:timestamp`

}

type webhookInvoked struct {

  Event string `json:event`
  Params string `json:params`
  Timestamp string `json:timestamp`

}

type status struct {

  Gitlab string `json:gitlab`
  DB string `json:db`
  Version string `json:version`
  Uptime string `json:uptime`

}
