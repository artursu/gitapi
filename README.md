# GITAPI README #





** /Commits **

Accepts empty payload
Query parameters are: limit and auth
Example:
/commits?limit=10?auth=whatever





** /Languages **

Not implemented






** /Issues **

Query parameters are: type and auth
type can be either "users" or "labels"
Requires body with project name

{ 
"project": "project name" //Project to collect data about
}

Example:
/issues?type=users&auth=whatever
	with body:
	{"project": "IMT2681"}


** /Status **

Accepts empty payload
Example:
/status





** /Webhooks **

Requires body with webhook data if POST-ing

{ 
	"event": "eventName" //One of the following: {"commits", "languages", "issues", "status"}
	"url": "address" //URL to invoke to
}

Requires no body and no query parameters if GET-ing
Might be used to get all webhooks or a specific one by specifiyng it's ID

Examples:
GET request: webhooks/ //Gets all webhooks
GET request: webhooks/aspecificID
POST request: webhooks/ //Registers a webhook
	with body:
	{ 
		"event": "eventName" //One of the following: {"commits", "languages", "issues", "status"}
		"url": "address" //URL to invoke to
	}
	
** Sorry for bad markup, I can't into Github markup **

** Notes: **

* name_with_namespace is used as project name, because it is actually unique
