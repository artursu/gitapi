package gitapi

import (

  "time"
  "net/http"

)

func HandlerLanguages (w http.ResponseWriter, r* http.Request) {

	invokeWebhooks ("languages", "", time.Now().String())

	http.Error(w, "501 Not Implemented", http.StatusNotImplemented)

}
